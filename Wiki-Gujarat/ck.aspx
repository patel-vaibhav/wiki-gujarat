﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="ck.aspx.cs" Inherits="ck" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="scripts/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="scripts/ckeditor/config.js" type="text/javascript"></script>
    <script src="scripts/ckeditor/build-config.js" type="text/javascript"></script>
    <script src="scripts/ckeditor/styles.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <asp:Panel ID="Panel1" runat="server">
     <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <CKEditor:CKEditorControl ID="editor1" runat="server" BasePath="/scripts/ckeditor/">
                                
            </CKEditor:CKEditorControl>
            <asp:Button ID="Button2" runat="server" Text="Button" />
    </asp:Panel>

     <asp:Panel ID="Panel2" runat="server">
     <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <CKEditor:CKEditorControl ID="CKEditorControl1" runat="server" BasePath="/scripts/ckeditor/">
                                
            </CKEditor:CKEditorControl>
            <asp:Button ID="Button1" runat="server" Text="Button" />
    </asp:Panel>

     <asp:Panel ID="Panel3" runat="server">
     <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <CKEditor:CKEditorControl ID="CKEditorControl2" runat="server" BasePath="/scripts/ckeditor/">
                                
            </CKEditor:CKEditorControl>
            <asp:Button ID="Button3" runat="server" Text="Button" />
    </asp:Panel>
    
    
</asp:Content>
