﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminDashboard.aspx.cs" Inherits="AdminDashboard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Admin Page</title>
  <link rel="stylesheet" href="css/admin.css" type="text/css" />
</head>

<body>

<form id="form1" runat="server">
<table align="center" class="table">


<!-- for Header -->

    <tr>
    <td>
        <table class="table">
        <tr>

        <td align="left"><span style="font: 40px Impact, Charcoal, sans-serif">Wiki Gujarat</span></td>

        <td align="right">

            <table>
            <tr>
            <td><img src="images/admin_icon.jpg" alt="admin" /></td>
            <td align="left">Welcome,<font color="Red"><b> Dhruv Patel </font><br><a href="">LOGOUT</a></b></td>
            </tr>
            </table>

        </td>

        </tr>
        </table>

    </td>
    </tr>

<!-- for Navigation -->

    <tr>
    <td>
        <table class="nav1">

        <tr>
        <td>
            <ul>
            <li><a href="link2">Dashboard</a></li>
            <li><a href="link3">Add User</a></li>
            <li><a href="link4">Edit User</a></li>
            <li><a href="link4">Edit User Privilege</a></li>
            <li><a href="link4">Edit Page Privilege</a></li>
            <li><a href="link5">Users</a></li>
            </ul>
        </td>
        </tr>

        </table>
    </td>
    </tr>

<!-- for Welcome Note -->

    <tr>

    <td>
    <h1>Welcome to the Admin Panel of Wiki Gujarat</h1>
    </td>

    </tr>


<!-- for space -->

    <tr><td class="space"></td></tr>

<!-- for form -->

    <tr>
        <td class="table">
            <table class="grayboxborder" cellpadding="0px" cellspacing="0px"class="formtab">

                <tr>
                <td>
                    <table class="gray table"width="1000px">
                    <tr>
                        <td align="left">Add New User</td>
                        <td align="right"><a href="">View all Users</a></td>
                    </tr>
                    </table>
                </td>
                </tr>

                <tr>
                <td>
                    <table class="errortab" >

                    <tr>
                        <td  class="errormsg">
                        <table>
                        <tr>
                            <td><img src="images/error_message.jpg" alt="error" />&nbsp;</td>
                            <td>Error notification</td>
                        </tr>
                        </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="space"></td>
                    </tr>

                    <tr>
                    <td>
                        <table style="width: 970px ; " class="grayboxborder" cellpadding="0" cellspacing="0">
                        <tr>
                        <td class="form">Add new property</td>
                        </tr>

                        <tr>
                        <td>
                            <table class="innertab">

                            <tr>
                            <td align="right">Username : </td>
                            <td align="left"><input type="text" class="textfield"/></td>
                            </tr>

                            <tr>
                            <td align="right">Password : </td>
                            <td align="left"><input type="text" class="textfield" /></td>
                            </tr>

                            <tr>
                            <td align="right">Email Address : </td>
                            <td align="left"><input type="text" class="textfield" /></td>
                            </tr>

                            <tr>
                            <td align="right">Country : </td>
                            <td align="left"><input type="text" class="textfield" /></td>
                            </tr>

                            <tr>
                            <td align="right">State : </td>
                            <td align="left"><input type="text" class="textfield" /></td>
                            </tr>

                            <tr>
                            <td align="right">City : </td>
                            <td align="left"><input type="text" class="textfield" /></td>
                            </tr>

                            <tr>
                            <td align="right">Mobile : </td>
                            <td align="left"><input type="text"  class="textfield"/></td>
                            </tr>



                            <tr>
                            <td></td>
                            <td align="left"><input type="button" value="Submit" class="inputbutton"/>
                            <input type="button" value="Cancel" class="inputbutton" /></td>
                            </tr>

                            </table>
                        </td>
                        </tr>



                        </table>
                    </td>
                    </tr>




                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr></tr>
    <tr>
        <td class="footer copyright" align="right">Copyright @ 2014 Wiki-Gujarat . All rights reserved.</td>
    </tr>
</table>
</form>
</body>

</html>
