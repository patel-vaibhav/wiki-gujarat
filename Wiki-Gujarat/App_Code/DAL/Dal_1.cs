﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for Dal_1
/// </summary>
public class Dal_1
{
    dbconnection d = new dbconnection();
	public Dal_1()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public void insert(string Name,string Password,string EmailId,string Country, string State, string City,int Mobile,int PrivilageLevel)
    {
        d.con.Close();
        d.con.Open();
        SqlCommand cmd = new SqlCommand("SP_Insert_Registration", d.con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Name", Name);
        cmd.Parameters.Add("@Password", Password);
        cmd.Parameters.Add("@EmailId", EmailId);
        cmd.Parameters.Add("@Country", Country);
        cmd.Parameters.Add("@State", State);
        cmd.Parameters.Add("@City", City);
        cmd.Parameters.Add("@Mobile", Mobile);
        cmd.Parameters.Add("@PrivilageLevel", PrivilageLevel);
        cmd.ExecuteNonQuery();
        d.con.Close();
    }

    public void insertcategory(string Category)
    {
        d.con.Close();
        d.con.Open();
        string query = "insert into Category_Data (Name) values (@Name)";
       
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = d.con;
        cmd.CommandText = query;
        cmd.Parameters.Add("@Name", Category);
       
        cmd.ExecuteNonQuery();
        d.con.Close();
    }

    public void insert1(string Title)
    {
        d.con.Close();
        d.con.Open();
        SqlCommand cmd = new SqlCommand("SP_Insert_Page_Data", d.con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Title", Title);
        cmd.Parameters.Add("@SubTitle1", "");
        cmd.Parameters.Add("@Content1", "");
        cmd.Parameters.Add("@SubTitle2", "");
        cmd.Parameters.Add("@Content2", "");
        cmd.Parameters.Add("@SubTitle3", "");
        cmd.Parameters.Add("@Content3", "");
        cmd.Parameters.Add("@SubTitle4", "");
        cmd.Parameters.Add("@Content4", "");
        cmd.Parameters.Add("@SubTitle5", "");
        cmd.Parameters.Add("@Content5", "");
        cmd.Parameters.Add("@SubTitle6", "");
        cmd.Parameters.Add("@Content6", "");
        cmd.Parameters.Add("@SubTitle7", "");
        cmd.Parameters.Add("@Content7", "");
        cmd.Parameters.Add("@SubTitle8", "");
        cmd.Parameters.Add("@Content8", "");
        cmd.Parameters.Add("@SubTitle9", "");
        cmd.Parameters.Add("@Content9", "");
        cmd.Parameters.Add("@SubTitle10", "");
        cmd.Parameters.Add("@Content10", "");
       
        cmd.ExecuteNonQuery();
        d.con.Close();
    }
// insert the delete request

    public void insertDeleteRequest(string title,string category,string section,string comments,string requestor)
    {
        d.con.Close();
        d.con.Open();
       
        SqlCommand cmd = new SqlCommand("SP_DeleteRequest_Insert", d.con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@PageName", title);
        cmd.Parameters.Add("@PageCategory", category);
        cmd.Parameters.Add("@Requestor_Name",requestor );
        cmd.Parameters.Add("@Requested_Name", "");
        cmd.Parameters.Add("@Comments", comments);
        cmd.Parameters.Add("@Status", "Pending");
        cmd.Parameters.Add("@Requested_Time", DateTime.Now);
        cmd.Parameters.Add("@SectionToDelete", section);

        cmd.ExecuteNonQuery();
        d.con.Close();
    }

    public void update(string country, string state, string city, int id)
    {
        d.con.Close();
        d.con.Open();
        SqlCommand cmd = new SqlCommand("Sp_Country_Update", d.con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Country", country);
        cmd.Parameters.Add("@State", state);
        cmd.Parameters.Add("@City", city);
        cmd.Parameters.Add("@ID", id);
        cmd.ExecuteNonQuery();
        d.con.Close();
    }

    public void update1(string SubTitle1, string Content1 ,string Title,int index)
    {
        d.con.Close();
        d.con.Open();
        string str1 = "@SubTitle" + index;
        string str2 = "@Content" + index;
        string str3 = "SubTitle" + index;
        string str4 = "Content" + index;
        string query = "update Page_Data set "+str3+" = '" + SubTitle1 + "' , " + str4 + " = '" + Content1 + "' where Title= '" + Title + "' ";
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = d.con;
        cmd.CommandText = query;
       
       // cmd.Parameters.Add(str1, SubTitle1);
        //cmd.Parameters.Add(str2, Content1);
       // cmd.Parameters.Add("@Title", Title);
       
        
        cmd.ExecuteNonQuery();
        d.con.Close();
    }

    public void delete(int id)
    {
        d.con.Close();
        d.con.Open();
        SqlCommand cmd = new SqlCommand("Sp_Country_Delete", d.con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@ID", id);
        cmd.ExecuteNonQuery();
        d.con.Close();
    }
}