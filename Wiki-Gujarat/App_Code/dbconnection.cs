﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for dbconnection
/// </summary>
public class dbconnection
{
    string str;
    public SqlConnection con = new SqlConnection();
    public dbconnection()
    {
        str = ConfigurationManager.ConnectionStrings["tempconnectionstring"].ConnectionString;
        con.ConnectionString = str.ToString();
        //
        // TODO: Add constructor logic here
        //
    }
    public void gridbind(string str, GridView grd)
    {
        SqlDataAdapter adp = new SqlDataAdapter(str, con);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        grd.DataSource = dt;
        grd.DataBind();
    }
    public void executequery(string str)
    {
        SqlCommand cmd = new SqlCommand(str, con);
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();    
    }
    public DataTable selectTable(string str)
    {
        SqlDataAdapter adp = new SqlDataAdapter(str, con);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        return dt;
    }
}