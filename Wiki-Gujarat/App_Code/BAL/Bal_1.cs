﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Bal_1
/// </summary>
public class Bal_1
{
    public string Name;
    public string Password;
    public string EmailId;
    public string Country;
    public string State;
    public string City;
    public int Mobile;
    public int PrivilageLevel;

    public string SubTitle1;
    public string Content1;
    public string Category;

	public Bal_1()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string sname
    {
        get
        {
            return Name;
        }
        set
        {
            Name = value;
        }
    }
    public string spassword
    {
        get
        {
            return Password;
        }
        set
        {
            Password = value;
        }
    }
    public string semailid
    {
        get
        {
            return EmailId;
        }
        set
        {
            EmailId = value;
        }
    }
    public string scountry
    {
        get
        {
            return Country;
        }
        set
        {
            Country = value;
        }
    }
    public string sstate
    {
        get
        {
            return State;
        }
        set
        {
            State = value;
        }
    }
    public string scity
    {
        get
        {
            return City;
        }
        set
        {
            City = value;
        }
    }

    public int smobile
    {
        get
        {
            return Mobile;
        }
        set
        {
            Mobile = value;
        }
    }
    public int sprivilagelevel
    {
        get
        {
            return PrivilageLevel;
        }
        set
        {
            PrivilageLevel = value;
        }
    }


    public string sSubTitle1
    {
        get
        {
            return SubTitle1;
        }
        set
        {
            SubTitle1 = value;
        }
    }


    public string sContent1
    {
        get
        {
            return Content1;
        }
        set
        {
            Content1 = value;
        }
    }

    public string sCategory
    {
        get
        {
            return Category;
        }
        set
        {
            Category = value;
        }
    }


    //for RequestDelete Data
    string Title;
    string Comments;
    string Section;

    public string sSection
    {
        get
        {
            return Section;
        }
        set
        {
            Section = value;
        }
    }

    public string sTitle
    {
        get
        {
            return Title;
        }
        set
        {
            Title = value;
        }
    }
    public string sComments
    {
        get
        {
            return Comments;
        }
        set
        {
            Comments = value;
        }
    }
}