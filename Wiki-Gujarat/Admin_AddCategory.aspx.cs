﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_AddCategory : System.Web.UI.Page
{
    dbconnection d = new dbconnection();
    Dal_1 dal = new Dal_1();
    Bal_1 en = new Bal_1();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btaddcategory_Click(object sender, EventArgs e)
    {
        DataTable dt = d.selectTable("select Name from Category_Data where Name='" + txtcategory.Text + "'");

        if (dt.Rows.Count == 0)
        {
           
            en.sCategory = txtcategory.Text;
            dal.insertcategory(en.sCategory);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Success", "alert('New Category Created');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Error", "alert('Category With Same Name Already Exists');", true);
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (Button1.Text == "Show Existing Categories")
        {
            GridCategory.DataSourceID = "SqlDataSource1";
            Button1.Text = "Hide Categories";
        }
        else if (Button1.Text == "Hide Categories")
        {
            GridCategory.DataSourceID = "";
            Button1.Text = "Show Existing Categories";

        }
    }
}