﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .txtdiv
        {
            font-size: 17px;
            margin-top: 20px;
        }
        
        .txtbox
        {
            border: none;
            background-color: #EDEDED;
            width: 300px;
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_content">
        <div class="content">
            <div>
                <span class="main_title">Log in</span><hr />
            </div>
            <div class="txtdiv">
                Username :
                <br />
                <br />
                <asp:TextBox ID="txtlogin" runat="server" class="txtbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtlogin" ErrorMessage="Username can not be empty."></asp:RequiredFieldValidator>
            </div>
            <div class="txtdiv">
                Password :
                <br />
                <br />
                <asp:TextBox ID="txtpass" runat="server" class="txtbox" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtpass" ErrorMessage="Password can not be empty."></asp:RequiredFieldValidator>
            </div>
            <div style="text-align: left; margin-left: 170px">
                <a href="">Forgot Password ?</a><br />
                <br />
                <br />
            </div>
            <div>
                <asp:Button ID="Button2" runat="server" Text="Log In" Style="border-radius: 10px;
                    background-color: #00af89; width: 300px; height: 30px; color: #fff; font-size: 15px;
                    font-weight: bold; cursor: pointer" OnClick="Button2_Click" />
                <asp:Label ID="lbstatus" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
