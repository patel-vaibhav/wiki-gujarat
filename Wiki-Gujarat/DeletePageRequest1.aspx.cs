﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class DeletePageRequest : System.Web.UI.Page
{
    dbconnection d = new dbconnection();
    Dal_1 dal = new Dal_1();
    Bal_1 en = new Bal_1();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            Session["UserSession1"] = "vaibhav";
            MultiView1.ActiveViewIndex = 0;
          //  DdPageTitle.Enabled = false;
            DdSection.Visible = false;
            lblselectsection.Visible = false;
        }
    }
    protected void DdCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DdPageTitle.Enabled = true ;
        DataTable dt = d.selectTable("select Title from FinalPageStatics where Category='" + DdCategory.SelectedValue + "'");
        DdPageTitle.DataSource = dt;
        DdPageTitle.DataTextField = "Title";
        DdPageTitle.DataBind();

    }

    protected void RadioSectionDelete_CheckedChanged(object sender, EventArgs e)
    {
        if (RadioSectionDelete.Checked == true)
        {
            DdSection.Visible = true;
            lblselectsection.Visible = true;
            DataTable dt = d.selectTable("select SectionCount from  FinalPageStatics where Title='" + DdPageTitle.SelectedValue + "'");
            if (dt.Rows.Count != 0)
            {
                int count = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
                String[] str1 = new string[count];

                for (int i = 1; i <= count; i++)
                {
                    DataTable dt1 = d.selectTable("select SubTitle" + i + " from  FinalPageData where Title='" + DdPageTitle.SelectedValue + "'");
                    str1[i - 1] = dt1.Rows[0].ItemArray[0].ToString();
                }

                DdSection.DataSource = str1;
                // DdSection.DataTextField = "SubTitle";
                DdSection.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Error", "alert('Please Select the Page Title To Delete');", true);
            }

        }
        else if (RadioWholeDelete.Checked == true)
        {
            DdSection.Visible = false;
            lblselectsection.Visible = false;
        }
    }
    protected void btdelete_Click(object sender, EventArgs e)
    {
        if (DdPageTitle.SelectedValue.ToString() != "")
        {
            if (RadioSectionDelete.Checked == true || RadioWholeDelete.Checked == true)
            {
                if (txtcomments.Text != "")
                {
                    lbtitle.Text = DdPageTitle.SelectedValue;
                    lbcategory.Text = DdCategory.SelectedValue;
                    if (RadioSectionDelete.Checked == true)
                    {
                        lbsection.Text = DdSection.SelectedValue;
                    }
                    else
                    {
                        lbsection.Text = "Request Delete Whole Page";
                    }
                    DataTable dt = d.selectTable("select PageViews,PrivilegeLevel from  FinalPageStatics where Title='" + DdPageTitle.SelectedValue + "'");
                    lbcount.Text = dt.Rows[0].ItemArray[0].ToString();
                    lbprivilege.Text = dt.Rows[0].ItemArray[1].ToString();
                    lbcomments.Text = txtcomments.Text;
                    MultiView1.ActiveViewIndex = 1;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Error", "alert('Please Give Reason To Delete');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Error", "alert('Please Select the Whole Page or Section To Delete ');", true);
            }
            
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Error", "alert('Please Select the Page Title To Delete');", true);
        }
    }
    protected void btconfirmdelete_Click(object sender, EventArgs e)
    {
        en.sTitle = DdPageTitle.SelectedValue;
        en.sCategory = DdCategory.SelectedValue;
        en.sSection = lbsection.Text;
        en.sComments = txtcomments.Text;
        string requestor=Session["UserSession1"].ToString();
       
        dal.insertDeleteRequest(en.sTitle, en.sCategory,en.sSection, en.sComments,requestor);
        MultiView1.ActiveViewIndex = 2;
    }
    protected void btback_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void btnavigate_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx?Name=ahmedabad");
    }
}