﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;


public partial class CreatePage : System.Web.UI.Page
{
    dbconnection d = new dbconnection();
    Dal_1 dal = new Dal_1();
    Bal_1 en = new Bal_1();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.ActiveViewIndex = 0;
            //pn1.Enabled = false;
        }
        //else
        //{
        //    this.createcontrols();
        //}
       
    }

    //private void createcontrols()
    //{
    //    int count = Convert.ToInt32(DropDownList1.SelectedValue);


    //    for (int i = 1; i <= count; i++)
    //    {
    //        TextBox txt1 = new TextBox();
    //        txt1.Text = "Title" + i.ToString() + "\n";
    //        HtmlGenericControl lineBreak = new HtmlGenericControl("br");
    //        pncontent.Controls.Add(txt1);
    //        pncontent.Controls.Add(lineBreak);

    //        //  Label hr = new Label();
    //        HtmlGenericControl lineBreak4 = new HtmlGenericControl("hr");
    //        pncontent.Controls.Add(lineBreak4);
    //        //hr.Text = "______________________________________________________________________________\n";
    //        // pncontent.Controls.Add(hr);
    //        HtmlGenericControl lineBreak1 = new HtmlGenericControl("br");
    //        pncontent.Controls.Add(lineBreak1);

    //        Label content = new Label();
    //        content.Text = "content" + i.ToString() + "\n";
    //        pncontent.Controls.Add(content);
    //        HtmlGenericControl lineBreak2 = new HtmlGenericControl("br");
    //        pncontent.Controls.Add(lineBreak2);

    //        Button bt1 = new Button();
    //        bt1.Text = "Edit" + i.ToString() + "\n";
    //        pncontent.Controls.Add(bt1);
    //        bt1.Click += new EventHandler(this.bt1_Click);

    //        HtmlGenericControl lineBreak5 = new HtmlGenericControl("br");
    //        pncontent.Controls.Add(lineBreak5);

    //    }
    //}

    protected void BtCreate_Click(object sender, EventArgs e)
    {
        DataTable dt = d.selectTable("select Title from Page_Data where Title='" + txttitle.Text + "'");
        if (dt.Rows.Count == 0)
        {


            dal.insert1(txttitle.Text);

            lbtitle.Text = txttitle.Text;
            
            //pn1.Enabled = false;

            // adding panel control

            //int count = Convert.ToInt32(DropDownList1.SelectedValue);


            //    for (int i = 1; i <= count; i++)
            //    {
            //        Label lbl = new Label();
            //        lbl.Text = "LABEL" + i.ToString() + "\n";
            //        HtmlGenericControl lineBreak = new HtmlGenericControl("br");
            //        pncontent.Controls.Add(lbl);
            //        pncontent.Controls.Add(lineBreak);

            //      //  Label hr = new Label();
            //        HtmlGenericControl lineBreak4 = new HtmlGenericControl("hr");
            //        pncontent.Controls.Add(lineBreak4);
            //        //hr.Text = "______________________________________________________________________________\n";
            //       // pncontent.Controls.Add(hr);
            //        HtmlGenericControl lineBreak1 = new HtmlGenericControl("br");
            //        pncontent.Controls.Add(lineBreak1);

            //        Label content = new Label();
            //        content.Text = "content" + i.ToString()+"\n";
            //        pncontent.Controls.Add(content);
            //        HtmlGenericControl lineBreak2 = new HtmlGenericControl("br");
            //        pncontent.Controls.Add(lineBreak2);


            //    }

            int count = Convert.ToInt32(DropDownList1.SelectedValue);
            MultiView1.ActiveViewIndex = 1;

            if (count == 1)
            {

                Panel1.Enabled = true;
                Panel2.Enabled = false;
                Panel3.Enabled = false;
                Panel1.Visible = true;
                Panel2.Visible = false;
                Panel3.Visible = false;
            }
            else if (count == 2)
            {
                Panel1.Enabled = true;
                Panel2.Enabled = true;
                Panel3.Enabled = false;
                Panel1.Visible = true;
                Panel2.Visible = true;
                Panel3.Visible = false;
            }
            else
            {
                Panel1.Enabled = true;
                Panel2.Enabled = true;
                Panel3.Enabled = true;
                Panel1.Visible = true;
                Panel2.Visible = true;
                Panel3.Visible = true;
            }

        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Error", "alert('Page with this title already Exists.');",true);
        }


    }

    //protected void bt1_Click(Object sender, EventArgs e)
    //{
    //    string Title1=txttitle1.Text;
    //    string Content1=editor1.Text;
    //    buttonClick(Title1, Content1, 1);
    //}

    public void buttonClick(String SubTitle,string Content,int index)
    {
        
        en.sSubTitle1 = SubTitle;
        en.sContent1 = Content;
        dal.update1(en.sSubTitle1, en.sContent1,lbtitle.Text,index);
        
    }
    protected void Btupload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            string fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName);

            if (fileExtension.ToLower() != ".gif" && fileExtension.ToLower() != ".jpeg" && fileExtension.ToLower() != ".png" && fileExtension.ToLower() != ".jpg")
            {
                lblupmessage.Text = "only .gif, .jpeg, .jpg or .png file supported";
                lblupmessage.ForeColor = System.Drawing.Color.Red;
            }
            else
            {

                int fileSize = FileUpload1.PostedFile.ContentLength;
                if (fileSize > 1048576)
                {
                    lblupmessage.Text = "Maximum Size 1 MB only";
                    lblupmessage.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    FileUpload1.SaveAs(Server.MapPath("~/uploads/" + lbtitle.Text + fileExtension));
                    lblupmessage.Text = "file uploaded successfully";
                    lblupmessage.ForeColor = System.Drawing.Color.Green;
                    Image1.ImageUrl = "uploads/" + lbtitle.Text + fileExtension;
                }
            }
        }
        else
        {
            lblupmessage.Text = "Please Select A File";
            lblupmessage.ForeColor = System.Drawing.Color.Red;
        }

       

    }

    protected void bt1_Click1(object sender, EventArgs e)
    {
        string Title1 = txttitle1.Text;
        string Content1 = editor1.Text;
        buttonClick(Title1, Content1, 1);
        Panel1.BackColor = System.Drawing.Color.Green;
        editor1.BackColor = System.Drawing.Color.Green;
        Panel1.ForeColor = System.Drawing.Color.Red;
        editor1.ForeColor = System.Drawing.Color.Red;
        

    }
    protected void bt2_Click(object sender, EventArgs e)
    {
        string Title1 = txttitle2.Text;
        string Content1 = editor2.Text;
        buttonClick(Title1, Content1, 2);
    }
    
    protected void bt3_Click(object sender, EventArgs e)
    {
        string Title1 = txttitle3.Text;
        string Content1 = editor3.Text;
        buttonClick(Title1, Content1, 3);
    }
}