﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

//[System.Web.Script.Services.ScriptMethod()]
//[System.Web.Services.WebMethod]



public partial class mainpage : System.Web.UI.Page
{

    dbconnection d = new dbconnection();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        
        
        DataTable dt = d.selectTable("select Title from PageTitle where Title='" + txtsearch.Text + "'");
       
    }
    protected void Btsearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx?Name=" + txtsearch.Text);
    }
    protected void Btcategory_Click(object sender, EventArgs e)
    {
        Response.Redirect("portal.aspx");
    }

    //[System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
    //public static string[] GetCountries(string prefixText, int count, string contextKey)
    //{
    //    return default(string[]);
    //}

    [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
    public static string[] GetCompletionList(string prefixText, int count, string contextKey)
    {
        dbconnection d = new dbconnection();
        
        DataTable dt1 = d.selectTable("select Title from PageTitle");
        int len=dt1.Rows.Count;
        string []titles= new string[len];
        for(int i=0;i<len;i++)
        {
            titles[i]=dt1.Rows[i].ItemArray[0].ToString();
        }

        return (from t in titles where t.StartsWith(prefixText,StringComparison.CurrentCultureIgnoreCase) select t).Take(count).ToArray();
    }
}