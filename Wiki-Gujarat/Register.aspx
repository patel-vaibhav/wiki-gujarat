﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            font-size: 17px;
            margin-top: 20px;
        }
        .style2
        {
            border: none;
            background-color: #EDEDED;
            width: 300px;
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_content">
        <div class="content">
            <div>
                <span class="main_title">Registration</span><hr />
            </div>
            <br />
            <div>

                <table style="margin-bottom: 20px">
                    <tr style="padding: 10px">
                        <td align="right" style="padding: 10px">
                            <span style="color: red; padding: 3px">*</span>Username :
                        </td>
                        <td style="padding: 10px">
                            <asp:TextBox ID="txtname" runat="server" class="style2" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                               ControlToValidate="txtname" ErrorMessage="Username cannot be blank"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr style="padding: 10px">
                        <td align="right" style="padding: 10px">
                            <span style="color: red; padding: 3px">*</span>Password :
                        </td>
                        <td style="padding: 10px">
                            <asp:TextBox ID="txtpassword" runat="server" class="style2" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="txtpassword" ErrorMessage="Password cannot be blank"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr style="padding: 10px">
                        <td align="right" style="padding: 10px">
                            <span style="color: red; padding: 3px">*</span>Confirm Password :
                        </td>
                        <td style="padding: 10px">
                            <asp:TextBox ID="txtconfirmpass" runat="server" class="style2" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                               ControlToValidate="txtconfirmpass" ErrorMessage="Confirm Password cannot be blank"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr style="padding: 10px">
                        <td align="right" style="padding: 10px">
                            <span style="color: red; padding: 3px">*</span>Email :
                        </td>
                        <td style="padding: 10px">
                            <asp:TextBox ID="txtemail" runat="server" class="style2" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                               ControlToValidate="txtemail" ErrorMessage="Email cannot be blank"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                </table>


                <table style="padding: 20px; border-top: 1px #08185E solid; margin-left: 40px">
                
                    <tr style="padding: 10px">
                        <td align="right" style="padding: 10px">
                            Country :
                        </td>
                        <td style="padding: 10px">
                            <asp:TextBox ID="txtcountry" runat="server" class="style2" />
                        </td>
                    </tr>
                
                    <tr style="padding: 10px">
                        <td align="right" style="padding: 10px">
                            State :
                        </td>
                        <td style="padding: 10px">
                            <asp:TextBox ID="txtstate" runat="server" class="style2" />
                        </td>
                    </tr>
                
                    <tr style="padding: 10px">
                        <td align="right" style="padding: 10px">
                            City :
                        </td>
                        <td style="padding: 10px">
                            <asp:TextBox ID="txtcity" runat="server" class="style2" />
                        </td>
                    </tr>
                
                    <tr style="padding: 10px">
                        <td align="right" style="padding: 10px">
                            Mobile No. :
                        </td>
                        <td style="padding: 10px">
                            <asp:TextBox ID="txtmobile" runat="server" class="style2" 
                                 />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="margin: 20px 20px 20px 170px">
                <asp:Button ID="Button1" runat="server" Text="Register" Style="border-radius: 10px;
                    background-color: #00af89; width: 300px; height: 30px; color: #fff; font-size: 15px;
                    font-weight: bold; cursor: pointer" OnClick="Button1_Click" />
                <asp:Label ID="lbstatus" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
