﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mainpage.aspx.cs" Inherits="mainpage" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="margin: 0 auto; background-image: url(images/bg3.jpg);">
    <form runat="server">
    <div class="right_content">
        <div style="" align="center">
            <div style="margin-top: 50px">
                <img src="images/guj logo.jpg" alt="" /><br/><h1 style="margin:0px;padding:0px;color:Lime;font-family:Verdana;margin-top:20px">Wiki Gujarat</h1></div>
            <div style="position: relative; margin-top: 150px">
               
               
               <ajax:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </ajax:ToolkitScriptManager>

                <asp:TextBox ID="txtsearch" runat="server" Style="width: 300px; height: 30px; border: none;
                    border-radius: 20px; padding: 10px; font-size: 20px; text-align: center"></asp:TextBox>

                <ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtsearch"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000"
                    ServiceMethod="GetCompletionList" UseContextKey="True">
                </ajax:AutoCompleteExtender>

            </div>
            <div style="float: left; margin: 20px 100px 20px 600px">
                <asp:Button ID="Btsearch" runat="server" Text="Search" Style="padding: 10px; margin-right: 20px;
                    border: none; border-radius: 10px; cursor: pointer" value="Search" OnClick="Btsearch_Click" />
                <asp:Button ID="Btcategory" runat="server" Text="Category Portal" Style="padding: 10px;
                    border: none; border-radius: 10px; cursor: pointer" value="Catagory" OnClick="Btcategory_Click" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
