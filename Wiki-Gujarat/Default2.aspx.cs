﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            ListItem[] item1 = new ListItem[30];
            for (int i = 1; i <= 30; i++)
            {
                item1[i-1] = ListItem.FromString(i.ToString());
            }
            DropDownList1.Items.AddRange(item1);
            DropDownList2.Items.AddRange(item1);
            DropDownList3.Items.AddRange(item1);
            DropDownList1.SelectedIndex = 0;
            DropDownList2.SelectedIndex = 0;
            DropDownList3.SelectedIndex = 0;
            DropDownList1.ClearSelection();
            DropDownList2.ClearSelection();
            DropDownList3.ClearSelection();
            ViewState["check"] = "hi";
            ViewState["last"] = "";
            
            
        }
    }
    public int count = 0;
    
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        if (Convert.ToString(ViewState["check"].ToString()) == "hi")
        {
            ViewState["last"] = DropDownList1.SelectedValue.ToString();
            DropDownList2.Items.Remove(DropDownList1.SelectedValue);
            DropDownList3.Items.Remove(DropDownList1.SelectedValue);
            ViewState["check"] = "how";
        }
        else
        {
            string last = ViewState["last"].ToString();
            int index = Convert.ToInt32(last);
            DropDownList2.Items.Insert(index, last);
            DropDownList2.Items.Insert(index, last);
            ViewState["last"] = DropDownList1.SelectedValue;
            DropDownList2.Items.Remove(DropDownList1.SelectedValue);
            DropDownList3.Items.Remove(DropDownList1.SelectedValue);
            

        }

    }
    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList3.Items.Remove(DropDownList2.SelectedValue); 
    }
}