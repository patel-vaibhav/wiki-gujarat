﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class temp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { }
    }
    protected void Btupload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            string fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName);

            if (fileExtension.ToLower() != ".gif" && fileExtension.ToLower() != ".jpeg" && fileExtension.ToLower() != ".png" && fileExtension.ToLower() != ".jpg")
            {
                lblupmessage.Text = "only .gif, .jpeg, .jpg or .png file supported";
                lblupmessage.ForeColor = System.Drawing.Color.Red;
            }
            else
            {

                int fileSize = FileUpload1.PostedFile.ContentLength;
                if (fileSize > 1048576)
                {
                    lblupmessage.Text = "Maximum Size 1 MB only";
                    lblupmessage.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    FileUpload1.SaveAs(Server.MapPath("~/uploads/" + FileUpload1.FileName));
                    lblupmessage.Text = "file uploaded successfully";
                    lblupmessage.ForeColor = System.Drawing.Color.Green;
                }
            }
        }
        else
        {
            lblupmessage.Text = "Please Select A File";
            lblupmessage.ForeColor = System.Drawing.Color.Red;
        }
       
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        for (int i = 1; i <= 4; i++)
        {
            Label lbl = new Label();
            lbl.Text = "LABEL" + i.ToString() + "\n";
            pncontent.Controls.Add(lbl);

            Label hr = new Label();
            hr.Text = "__________________________________________________" + "\n" ;
            pncontent.Controls.Add(hr);

            Label content = new Label();
            content.Text = "content" + i.ToString() + "\n";
            pncontent.Controls.Add(content);



        }
    }
}