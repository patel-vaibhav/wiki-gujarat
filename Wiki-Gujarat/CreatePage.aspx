﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="CreatePage.aspx.cs" Inherits="CreatePage" %>
    <%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="scripts/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="scripts/ckeditor/config.js" type="text/javascript"></script>
    <script src="scripts/ckeditor/build-config.js" type="text/javascript"></script>
    <script src="scripts/ckeditor/styles.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="Label2" runat="server" Text="Title Of the Page"></asp:Label>
            <asp:TextBox ID="txttitle" runat="server"></asp:TextBox>
            <br />
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:tempconnectionstring %>" 
                SelectCommand="SELECT [Name] FROM [Category_Data]"></asp:SqlDataSource>
           
           
            <asp:Label ID="lblCategory" runat="server" Text="Select Category  :  "></asp:Label> 
            <asp:DropDownList ID="CategoryList" runat="server" 
                DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Name">

            </asp:DropDownList>
            <br />
            <asp:Label ID="lblOr" runat="server" Text="Or : Request For New Category Creation "></asp:Label>
            <asp:TextBox ID="txtCategory" runat="server"></asp:TextBox>
            <br />
           
            <asp:Label ID="Label1" runat="server" Text="No of Sections"></asp:Label>
            <asp:DropDownList ID="DropDownList1" runat="server" Style="margin-left: 28px">
                <%--nested data list--%>
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>5</asp:ListItem>
                <asp:ListItem>6</asp:ListItem>
                <asp:ListItem>7</asp:ListItem>
                <asp:ListItem>8</asp:ListItem>
                <asp:ListItem>9</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="BtCreate" runat="server" Text="Create" OnClick="BtCreate_Click" />
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="right_content">
                <div class="content">
                    <div>
                        
                            
                        <asp:Label ID="txtloginuser" runat="server"></asp:Label>
                       
                        <asp:Label ID="lbtitle" runat="server" Text="Label" class="main_title"></asp:Label><hr />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       
                    </div>
                    <div class="mainimg_box">
                        <div class="img_box">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="Btupload" />
                                </Triggers>
                                <ContentTemplate>
                                    <asp:Image ID="Image1" runat="server" Width="200px" Height="270px" />
                                    <asp:Button ID="Btupload" runat="server" Text="Upload" OnClick="Btupload_Click" />
                                    <asp:Label ID="lblupmessage" runat="server" Style="font-bold: true"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:FileUpload ID="FileUpload1" runat="server" Style="float: left" />
                        </div>
                    </div>
                    <asp:Panel ID="Panel1" runat="server">
                        <asp:TextBox ID="txttitle1" runat="server"></asp:TextBox>
                        <CKEditor:CKEditorControl ID="editor1" runat="server" BasePath="/scripts/ckeditor/">
                                
                        </CKEditor:CKEditorControl>
                        <asp:Button ID="bt1" runat="server" Text="Button" onclick="bt1_Click1" />
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server">
                        <asp:TextBox ID="txttitle2" runat="server"></asp:TextBox>
                        <CKEditor:CKEditorControl ID="editor2" runat="server" BasePath="/scripts/ckeditor/">
                                
                        </CKEditor:CKEditorControl>
                        <asp:Button ID="bt2" runat="server" Text="Button" onclick="bt2_Click" />
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server">
                        <asp:TextBox ID="txttitle3" runat="server"></asp:TextBox>
                        <CKEditor:CKEditorControl ID="editor3" runat="server" BasePath="/scripts/ckeditor/">
                                
                        </CKEditor:CKEditorControl>
                        <asp:Button ID="bt3" runat="server" Text="Button" onclick="bt3_Click" />
                    </asp:Panel>


                    <%--   <asp:Panel ID="pncontent" runat="server" Direction="LeftToRight" Style="margin-left: 250px">
                    </asp:Panel>--%>
                    <%--<div style="overflow: hidden; word-wrap: break-word; padding: 10px;">
                       <asp:Panel ID="pn1"  runat="server">     <asp:Label ID="lbc1" runat="server" Text="Label"></asp:Label><br/><br/>
                        <asp:Label ID="lbt2" runat="server" Text="Label12345"></asp:Label><hr/></asp:Panel>
                        <asp:Label ID="lbc2" runat="server" Text="Label"></asp:Label><br/><br/>
                        <asp:Label ID="lbt3" runat="server" Text="Label"></asp:Label><hr/>
                        <asp:Label ID="lbc3" runat="server" Text="Label"></asp:Label><br/><br/>
                        <asp:Label ID="lbt4" runat="server" Text="Label"></asp:Label><hr/>
                        <asp:Label ID="lbc4" runat="server" Text="Label"></asp:Label><br/><br/>
                        <asp:Label ID="lbt5" runat="server" Text="Label"></asp:Label><hr/>
                        <asp:Label ID="lbc5" runat="server" Text="Label"></asp:Label><br/><br/>
                        <asp:Label ID="lbt6" runat="server" Text="Label"></asp:Label><hr/>
                        <asp:Label ID="lbc6" runat="server" Text="Label"></asp:Label><br/><br/>
                        <asp:Label ID="lbt7" runat="server" Text="Label"></asp:Label><hr/>
                        <asp:Label ID="lbc7" runat="server" Text="Label"></asp:Label><br/><br/>
                        <asp:Label ID="lbt8" runat="server" Text="Label"></asp:Label><hr/>
                        <asp:Label ID="lbc8" runat="server" Text="Label"></asp:Label><br/><br/>
                        <asp:Label ID="lbt9" runat="server" Text="Label"></asp:Label><hr/>
                        <asp:Label ID="lbc9" runat="server" Text="Label"></asp:Label><br/><br/>
                        <asp:Label ID="lbt10" runat="server" Text="Label"></asp:Label><hr/>
                        <asp:Label ID="lbc10" runat="server" Text="Label"></asp:Label><br/><br/>
                        
                        </div>--%>
                </div>
            </div>
        </asp:View>
        <asp:View ID="View3" runat="server">
        </asp:View>
    </asp:MultiView>
    <br />
</asp:Content>
