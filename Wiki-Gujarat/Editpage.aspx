﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="Editpage.aspx.cs" Inherits="Editpage" ValidateRequest="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="scripts/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="scripts/ckeditor/config.js" type="text/javascript"></script>
    <script src="scripts/ckeditor/build-config.js" type="text/javascript"></script>
    <script src="scripts/ckeditor/styles.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form>
    <%--<asp:TextBox ID="editor1" runat="server" TextMode="MultiLine"></asp:TextBox>--%>
    <CKEditor:CKEditorControl ID="editor1" runat="server" BasePath="/scripts/ckeditor/">
    
    </CKEditor:CKEditorControl>
    <%--<textarea id="editor1" cols="20" rows="20"></textarea>--%>
    <%--<script type="text/javascript">        CKEDITOR.replace('ContentPlaceHolder1_editor1', { uiColor: '#14B8C4', removePlugins: 'link' }) ; CKEDITOR.replace('<%=editor1.ClientID %>')</script>
    --%>
    <%--<script type="text/javascript">    CKEDITOR.config.toolbar_Full =
[
{ name: 'document', items: ['Source'] },
{ name: 'clipboard', items: ['Cut' , 'Copy' ,'Paste', '-', 'Undo', 'Redo'] },
{ name: 'editing', items: ['Find'] },
{ name: 'basicStyles', items: ['Bold', 'Italic', 'UnderLine'] },
{ name: 'paragraph', items: ['JustifyLeft', 'JustifyRight', 'JustifyCenter'] }

];   
CKEDITOR.replace('<%= editor1.ClientID %>')</script>--%>
    <%--<script type="text/javascript">        CKEDITOR.instances['editor1'].setData(html); 
    </script>--%>
    <br />
    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
    <br />
    <br />
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </form>
</asp:Content>
