﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="DeletePageRequest1.aspx.cs" Inherits="DeletePageRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:MultiView ID="MultiView1" runat="server">
    <asp:View ID="View1" runat="server">
    <table style="margin-left:50px;padding:[20px][20px][20px][20px]">
    <tr>
    <td>
    <asp:Label ID="Label1" runat="server" Text="Select Category"></asp:Label>
    </td>
    <td>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:tempconnectionstring %>" 
            SelectCommand="SELECT [Name] FROM [Category_Data]"></asp:SqlDataSource>
    <asp:DropDownList ID="DdCategory" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="Name" DataValueField="Name" AutoPostBack="True" 
            onselectedindexchanged="DdCategory_SelectedIndexChanged">
    </asp:DropDownList>
    </td>
    </tr>
        

     <tr>
    <td>
    <asp:Label ID="Label2" runat="server" Text="Select Page"></asp:Label>
    </td>
    <td>
       
   
    <asp:DropDownList ID="DdPageTitle" runat="server" AutoPostBack="True">
    </asp:DropDownList>
    </td>
    </tr>

     <tr>
     
         
        
    <td>

    <asp:RadioButton ID="RadioWholeDelete"  runat="server" Text="Delete Whole Page" 
            GroupName="one" AutoPostBack="True"/>
    </td>
        
    <td>
    <asp:RadioButton ID="RadioSectionDelete" runat="server" 
            Text="Delete Section Of the Selected Page" GroupName="one" 
            oncheckedchanged="RadioSectionDelete_CheckedChanged" AutoPostBack="True"/>
    </td>
    
    </tr>

      <tr>
    <td>
    <asp:Label ID="lblselectsection" runat="server" Text="Select Section To Delete : "></asp:Label>
    </td>
    <td>
       
   
    <asp:DropDownList ID="DdSection" runat="server" AutoPostBack="True">
    </asp:DropDownList>
    </td>
    </tr>
      <tr>
    <td>
    <asp:Label ID="Label4" runat="server" Text="Comments/Reason To Delete  :  "></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtcomments" runat="server" Height="111px" TextMode="MultiLine" 
            Width="348px"></asp:TextBox>
   
    
    </td>
    </tr>

    </table>
     <asp:Button ID="btdelete" runat="server" Text="Request Delete" 
            onclick="btdelete_Click" style="margin-left: 261px" />

       
    </asp:View>
    <asp:View ID="View2" runat="server">
    <table>
    <tr><td>Page Title</td><td>:</td><td>
        <asp:Label ID="lbtitle" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Page Category</td><td>:</td><td><asp:Label ID="lbcategory" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Section To Delete</td><td>:</td><td><asp:Label ID="lbsection" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Page Count</td><td>:</td><td><asp:Label ID="lbcount" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Page Privilege Level</td><td>:</td><td><asp:Label ID="lbprivilege" runat="server" Text=""></asp:Label></td></tr>
    <tr><td>Comments/Reasons To Delate</td><td>:</td><td><asp:Label ID="lbcomments" runat="server" Text=""></asp:Label></td></tr>
  
    </table>
    <br/>
        <asp:Button ID="btconfirmdelete" runat="server" Text="Confirm Request For Delete" 
            style="margin-left: 149px" onclick="btconfirmdelete_Click" Width="208px" />
        <asp:Button ID="btback" runat="server" Text="Back" Width="81px" 
            style="margin-left: 40px" onclick="btback_Click"/>

    </asp:View>
    
          <asp:View ID="View3" runat="server">
          <center>
               <br/>
               <br/>
              <asp:Label ID="Label5" runat="server" Text="Your Request For Delete Has Been Accepted" Style="font-size:20px;color:Green"></asp:Label>
              <br/>
              <asp:Button ID="btnavigate" runat="server" Text="Go To Home Page" 
                  onclick="btnavigate_Click" />
              </center>
              
          </asp:View>
    
</asp:MultiView>
</asp:Content>

